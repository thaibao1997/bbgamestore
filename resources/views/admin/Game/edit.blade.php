@extends('admin.AdminLayout')

@section('content')

    <h1>Edit Game ID: {{$game->id}}</h1>
    <h3 class="alert-success">{{session('status')}}</h3>
    @include('errors.form_validate')

    {!! Form::model($game,['method'=>'PUT','route'=>['admin.game.update',$game->id],'enctype'=>'multipart/form-data']) !!}
    <div class="form-group"><label for="title">Name</label>
        {!! Form::text('name',null,['class'=>'form-control','placeholder'=>'','required'=>'']) !!}
    </div>

    <div class="form-group"><label for="Tags">Price</label>
        {!! Form::number('price',null,['class'=>'form-control','placeholder'=>'','required'=>'']) !!}
    </div>

    <div class="form-group"><label for="Tags">Public Year</label>
        @php
            $years=array();
        for($i=2000;$i<= date('Y') ;$i++)
            $years[(string)$i]=(string)$i;
        @endphp
        {!! Form::select('public_year',$years,date('Y') ,['class'=>'form-control','placeholder'=>'']) !!}
    </div>

    <div class="form-group"><label for="Tags">Company</label>

        {!! Form::text('company',null,['class'=>'form-control','placeholder'=>'']) !!}
    </div>

    <div class="form-group"><label for="image">Image</label>
        {!! Form::file('image',['class'=>'form-control','placeholder'=>'','onchange'=>"preview_image(event)"]) !!}
    </div>

    <div class="form-group"><label for="image">Current Image:</label>
        <img class="" src="{{ url($game->Images->first()->path) }}" alt="" width="150">
    </div>

    <div class="form-group"><label for="body">Description</label>
        {!! Form::textarea('description',null,['class'=>'form-control','cols'=>3,'required'=>'']) !!}
    </div>

    <div class="form-group"><label for="category">Category</label>
        @if(isset($categories))
            <ul class="list-group">
                @foreach($categories as $category)
                    <li  class="list-group-item col-xs-3" >
                        @if($game->Categories->find($category->id))
                            {!! Form::checkbox('categoriesID[]',$category->id,true) !!}
                        @else
                            {!! Form::checkbox('categoriesID[]',$category->id,false) !!}
                         @endif
                        {{$category->name}}
                    </li>

                @endforeach
            </ul>
        @endif
    </div>

    <div class="form-group col-lg-12"><label for="body">Keys</label>
        {!! Form::textarea('keys_',null,['class'=>'form-control','cols'=>3]) !!}
    </div>
    <br>
    <div class="" >
        {!! Form::submit('Add Game',['class'=>'btn btn-primary form-control']) !!}
        {!! Form::close() !!}
    </div>

@endsection