@extends('admin.AdminLayout')

@section('content')

    <h1>Add New Game</h1>

    <div class="col-xs-12">
        <h3 class="alert-success">{{session('status')}}</h3>
        @include('errors.form_validate')

        {!! Form::open(['method'=>'POST','route'=>'admin.game.create','enctype'=>'multipart/form-data']) !!}
        <div class="form-group"><label for="title">Name</label>
            {!! Form::text('name',null,['class'=>'form-control','placeholder'=>'','required'=>'']) !!}
        </div>

        <div class="form-group"><label for="Tags">Price</label>
            {!! Form::number('price',null,['class'=>'form-control','placeholder'=>'','required'=>'']) !!}
        </div>

        <div class="form-group"><label for="Tags">Public Year</label>
            @php
                $years=array();
            for($i=2000;$i<= date('Y') ;$i++)
                $years[(string)$i]=(string)$i;
            @endphp
            {!! Form::select('public_year',$years,date('Y') ,['class'=>'form-control','placeholder'=>'','required'=>'']) !!}
        </div>

        <div class="form-group"><label for="Tags">Company</label>

            {!! Form::text('company',null,['class'=>'form-control','placeholder'=>'']) !!}
        </div>

        <div class="form-group"><label for="image">Image</label>
            {!! Form::file('image',['class'=>'form-control','placeholder'=>'','onchange'=>"preview_image(event)",'required'=>'']) !!}
            <img id="output_image" width="150">
        </div>
        <div class="form-group"><label for="body">Description</label>
            {!! Form::textarea('description',null,['class'=>'form-control','cols'=>3,'required'=>'']) !!}
        </div>

        <div class="form-group"><label for="category">Category</label>
            @if(isset($categories))
                <ul class="list-group">
                    @foreach($categories as $category)
                        <li  class="list-group-item col-xs-3" >
                            {!! Form::checkbox('categoriesID[]',$category->id,false) !!}
                            {{$category->name}}
                        </li>

                    @endforeach
                </ul>
            @endif
        </div>
        <div class="form-group col-lg-12"><label for="body">Keys</label>
            {!! Form::textarea('keys',null,['class'=>'form-control','cols'=>3]) !!}
        </div>
        <br>
        <div class="" >
            {!! Form::submit('Add Game',['class'=>'btn btn-primary form-control']) !!}
            {!! Form::close() !!}
        </div>
    </div>

@endsection