@extends('admin.AdminLayout')

@section('content')
    <h1>Key Games</h1>
    <h3 class="alert-danger">{{ session('status')  }}</h3>

    <table class="table">
        <thead>
        <tr>
            <th>Game ID</th>
            <th>Game Name</th>
            <th>Key</th>
        </tr>
        </thead>
        <tbody>
        @if(isset($keys))
            @foreach($keys as $key)
                <tr>
                    <td>{{$key->Game()->withTrashed()->first()->id}}</td>
                    <td>{{$key->Game()->withTrashed()->first()->name}}</td>
                    <td>{{$key->key}}</td>
                    <td>
                        {!! Form::open(['method'=>'DELETE','route'=>['admin.game.key.delete',$key->id],'onsubmit' => 'return confirm("Are you sure?")']) !!}
                        {!! Form::submit('Delete',['class'=>'btn btn-danger']) !!}
                        {!! Form::close() !!}
                    </td>
                </tr>
            @endforeach
        @endif
        </tbody>
    </table>
@endsection