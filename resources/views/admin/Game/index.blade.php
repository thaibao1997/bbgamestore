@extends('admin.AdminLayout')

@section('content')

    <h1>All Games</h1>

    {{--<p>{{$users}}</p>--}}
    <h3 class="alert-danger">{{ session('status')  }}</h3>

    <table class="table">
        <thead>
        <tr>
            <th>ID</th>
            <th>Name</th>
            <th>Categories</th>
            <th>Price (VNĐ)</th>
            <th>Image</th>
            <th>Public Year</th>
            <th>Company</th>
            <th>Number Of Keys</th>
            <th>Description</th>
            <th>Added At</th>

        </tr>
        </thead>
        <tbody>
        @if($games)
            @foreach($games as $game)
                <tr>
                    <td>{{$game->id}}</td>
                    @php
                        $categories=$game->Categories;
                        $categoriesArr=Array();
                        foreach ($categories as $category)
                            array_push($categoriesArr,$category->name);
                        $categories=implode(', ',$categoriesArr);
                    @endphp
                    <td><a target="_blank" href="{{route('app.game',$game->id)}}">{{$game->name}}</a></td>
                    <td>{{$categories}}</td>
                    <td>{{ number_format($game->price)}}</td>
                    <td><img src="{{ url($game->Images->first()->path) }}" width="100"  alt=""></td>
                    <td>{{$game->public_year}}</td>
                    <td>{{$game->company}}</td>
                    <td>{{ $game->Keys->count() }}</td>
                    <td>{{substr($game->description,0,50) }}</td>
                    <td>{{$game->created_at->diffForHumans()}}</td>
                    <td>
                        <a href="{{route('admin.game.edit',$game->id)}}"><button class="btn btn-primary">Edit</button></a>
                    </td>
                    <td>
                        {!! Form::open(['method'=>'DELETE','route'=>['admin.game.delete',$game->id],'onsubmit' => 'return confirm("Are you sure?")']) !!}
                        {!! Form::submit('Delete',['class'=>'btn btn-danger']) !!}
                        {!! Form::close() !!}
                    </td>
            @endforeach
        @endif
        </tbody>
    </table>
@endsection