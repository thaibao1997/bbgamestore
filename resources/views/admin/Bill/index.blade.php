@extends('admin.AdminLayOut')
@php
    use Carbon\Carbon;
@endphp
@section('content')
    <h1>All Bills</h1>
    <table class="table">
        <thead>
        <tr>
            <th>ID</th>
            <th>User</th>
            <th>Games</th>
            <th>Money</th>
            <th>Created at</th>

        </tr>
        </thead>
        <tbody>
            @if(isset($bills))
                @foreach($bills as $bill)
                    <tr>
                        <td>{{$bill->id}}</td>
                        <td>{{  $bill->User()->withTrashed()->first()->email }}</td>
                        @php

                        @endphp
                        <td>
                            @foreach($bill->Keys()->withTrashed()->get() as $key)
                                {{$key->Game->name.": ".$key->key}} <br/>
                            @endforeach
                        </td>
                        <td>{{number_format($bill->money)}}</td>

                        <td>{{ $bill->created_at->format('h:i:s d/m/Y')}}</td>
                    </tr>
                @endforeach
            @endif

        </tbody>
    </table>
@endsection