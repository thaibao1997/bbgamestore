@extends("admin.AdminLayout")

@section('content')

    <h1>Categories</h1>
    <div class="col-sm-6">
        <h4 class="alert-success">{{session('success')}}</h4>
        <h4 class="alert-danger">{{session('unsuccess')}}</h4>
        <h4 class="alert-danger">{{session('delete')}}</h4>
        @include('errors.form_validate')
        <div>
            <label for="Add Category">Add Category</label>
            {!! Form::open(['method'=>'POST','route'=>'admin.category.store']) !!}
            <div class="form-group">
                {!! Form::text('name',null,['class'=>'form-control','placeholder'=>'','required'=>'']) !!}
            </div>
            <div class="form-group">
                {!! Form::submit('Add Category',['class'=>'btn btn-primary']) !!}
            </div>
            {!! Form::close() !!}
        </div>

        @if(isset($edit_category))
            <div>
                <label for="Edit Category ">Edit Category ID: {{$edit_category->id}}</label>
                {!! Form::model($edit_category,['method'=>'PUT','route'=>['admin.category.update',$edit_category->id]]) !!}
                <div class="form-group">
                    {!! Form::text('name',null,['class'=>'form-control','placeholder'=>'']) !!}
                </div>
                <div class="form-group">
                    {!! Form::submit('Edit Category',['class'=>'btn btn-primary']) !!}
                </div>
                {!! Form::close() !!}
            </div>
        @endif

    </div>
    <div class="col-sm-6">
        <table class="table">
            <thead>
            <tr>
                <th>ID</th>
                <th>Name</th>
            </tr>
            </thead>
            <tbody>
            @if($categories)
                @foreach($categories as $category)
                    <tr>
                        <td> <h4> {{$category->id}} </h4></td>
                        <td> <h4> {{$category->name}} </h4></td>
                        <td><a href="{{route('admin.category.edit',$category->id)}}" class="btn btn-primary">Edit</a>
                        </td>
                        <td>
                            {!! Form::open(['method'=>'DELETE','onsubmit'=>"return confirm('Are You Sure?')",'route'=>['admin.category.delete',$category->id]]) !!}
                            <div class="form-group">
                                {!! Form::submit('Delete',['class'=>'btn btn-danger']) !!}
                            </div>
                            {!! Form::close() !!}
                        </td>
                    </tr>
                @endforeach
            @endif
            </tbody>
        </table>
    </div>

@endsection