@extends('admin.AdminLayOut')

@section('content')
    <h1>All Comments</h1>
    <h3 class="alert-danger">{{ session('status')  }}</h3>
    <table class="table">
        <thead>
        <tr>
            <th>User</th>
            <th>Game</th>
            <th>Content</th>
            <th>Comment time</th>

        </tr>
        </thead>
        <tbody>
            @if(isset($comments))
                   @foreach($comments as $comment)
                        <tr>
                            <td>{{$comment->User->email}}</td>
                            <td><a href="{{route('app.game',$comment->game->id)}}">{{$comment->game->name}}</a></td>
                            <td>{{substr($comment->content,0,50)}}</td>
                            <td>{{$comment->created_at->diffForHumans()}}</td>
                            <td>
                                {!! Form::open(['method'=>'DELETE','action'=>['AdminCommentController@Destroy',$comment->id],'onsubmit' => 'return confirm("Are you sure?")']) !!}
                                {!! Form::submit('Delete',['class'=>'btn btn-danger']) !!}
                                {!! Form::close() !!}
                            </td>
                        </tr>
                   @endforeach
            @endif
        </tbody>
    </table>
@endsection