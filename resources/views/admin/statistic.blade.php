@extends('admin.AdminLayout')

@section('content')
        <div class="col-xs-6">
            <h2>Thống Kê Tình Hình Bán Hàng Theo Tháng</h2>
            <table class="table">
                <thead>
                <tr>
                    <th>Tháng</th>
                    <th>Số lượng đơn hàng</th>
                    <th>Tổng tiền (VNĐ)</th>
                </tr>
                </thead>
                <tbody>
                @if(isset($bills))
                    @php $total=0 @endphp
                    @foreach($bills as $bill)
                        @php $total+= $bill->total  @endphp
                        <tr>
                            <td>{{$bill->date}}</td>
                            <td>{{$bill->bill_count}}</td>
                            <td>{{number_format($bill->total)}}</td>
                        </tr>
                    @endforeach
                @endif
                <tr>
                    <td></td>
                    <td><h4>Tổng Cộng</h4></td>
                    <td><h4>{{number_format($total)}}</h4></td>
                </tr>

                </tbody>
            </table>
        </div>
        <div class="col-xs-6">
            <h2>Các Tình Hình Bán Các Game</h2>
            <table class="table">
                <thead>
                <tr>
                    <th>Game ID</th>
                    <th>Tên</th>
                    <th>Số Lượng Đã Mua</th>
                </tr>
                </thead>
                <tbody>
                @if(isset($games))
                    @php $total=0 @endphp
                    @foreach($games as $game)
                        @php $total+= $game['SL']  @endphp
                        <tr>
                            <td>{{$game['id']}}</td>
                            <td><a target="_blank" href="{{route('app.game',$game['id'])}}">{{$game['name']}}</a></td>
                            <td>{{$game['SL']}}</td>
                        </tr>
                    @endforeach
                @endif
                <tr>
                    <td></td>
                    <td><h4>Tổng Cộng</h4></td>
                    <td><h4>{{number_format($total)}}</h4></td>
                </tr>

                </tbody>
            </table>
        </div>
        <div class="col-lg-12" id="chart_tk"></div>
        <div class="col-lg-12" id="chart_tk2"></div>

@endsection

@section('header')
    @if(isset($bills))
        @php
            $string_chart_money="";
            $string_chart_count="";
            foreach ($bills as $bill){
               $string_chart_money.= "["."'".$bill->date."',".$bill->total.",".$bill->bill_count."],";
               $string_chart_count.= "["."'".$bill->date."',".$bill->bill_count."],";
            }
        $string_chart_money=trim($string_chart_money,',');
        @endphp
    @endif
    @if(isset($games))
        @php
            $pie_chart="";
            foreach ($games as $game){
               $pie_chart.= "["."\"".$game['name']."\",".$game['SL']."],";
            }
        $pie_chart=trim($pie_chart,',');
        @endphp
    @endif


    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript">
        google.charts.load('current', {'packages':['line', 'corechart']});
        google.charts.load('current', {'packages':['corechart']});
        google.charts.setOnLoadCallback(drawChart);

        function drawChart() {
            var chartDiv = document.getElementById('chart_tk');

            var data = new google.visualization.DataTable();
            data.addColumn('string', 'Month');
            data.addColumn('number', "Số Tiền");
            data.addColumn('number', "Số Đơn Hàng");

            data.addRows([
                {!! $string_chart_money !!}
            ]);

            var materialOptions = {
                chart: {
                    title: 'Thống Kê Số Tiền Và Số Đơn Hàng Theo Tháng'
                },
                width: 900,
                height: 500,
                series: {
                    // Gives each series an axis name that matches the Y-axis below.
                    0: {axis: 'SoTien'},
                    1: {axis: 'Don'}
                },
                axes: {
                    // Adds labels to each axis; they don't have to match the axis names.
                    y: {
                        SoTien: {label: 'VNĐ'},
                        Don: {label: 'Đơn'}
                    }
                }
            };

            var materialChart = new google.charts.Line(chartDiv);
            materialChart.draw(data, materialOptions);

            var dataPie = google.visualization.arrayToDataTable([
                ['Game', 'Số Lượng'],
                {!! $pie_chart !!}
            ]);

            var options = {
                title: 'Tình hình bán game theo số lượng',
                width:800,
                height:600
            };
            var pieChart = new google.visualization.PieChart(document.getElementById('chart_tk2'));
            pieChart.draw(dataPie, options);
        }

    </script>


@endsection