@extends("admin.AdminLayout")

@section('content')

    <h1>All Users</h1>

    {{--<p>{{$users}}</p>--}}
    <h3 class="alert-danger">{{ session('status')  }}</h3>

    <table class="table">
        <thead>
        <tr>
            <th>ID</th>
            <th>User Name</th>
            <th>Email</th>
            <th>Role</th>
            <th>Join Time</th>

        </tr>
        </thead>
        <tbody>
        @if($users)
            @foreach($users as $user)
                <tr>
                    <td>{{$user->id}}</td>
                    <td>{{$user->name}}</td>
                    <td>{{$user->email}}</td>
                    <td>{{$user->Role->name}}</td>
                    <td>{{$user->created_at->diffForHumans()}}</td>
                    <td>
                        {!! Form::open(['method'=>'DELETE','action'=>['AdminUserController@Destroy',$user->id],'onsubmit' => 'return confirm("Are you sure?")']) !!}
                        {!! Form::submit('Delete',['class'=>'btn btn-danger']) !!}
                        {!! Form::close() !!}
                    </td>
            @endforeach
        @endif
        </tbody>
    </table>
@endsection