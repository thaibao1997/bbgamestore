@extends('app.AppLayout')

@section('current_section')
    @if(Auth::Check())
        {{Auth::user()->name}}'s Profile
    @endif
@endsection

@section('content')
    <div class="col-md-8 col-md-offset-2">
        @include('errors.form_validate')
    @if(isset($user))
            <form class="form-horizontal" action="{{route('app.user.update')}}" method="post">
                {{csrf_field()}}
                {{method_field('PUT')}}
                <div class="form-group">
                    <label for="name" class="col-md-4 control-label">Name</label>
                    <div class="col-md-6">
                        <input type="text" name="name" value="{{$user->name}}" class="form-control" >
                    </div>
                </div>
                <div class="form-group">
                    <label for="name" class="col-md-4 control-label">Email</label>
                    <div class="col-md-6">
                        <input type="text" name="" value="{{$user->email}}" class="form-control" readonly>
                    </div>
                </div>
                <div class="form-group">
                    <label for="name" class="col-md-4 control-label">Role</label>
                    <div class="col-md-6">
                        <input type="text" name="" value="{{$user->role->name}}" class="form-control" readonly>
                    </div>
                </div>
                <div class="form-group">
                    <label for="name" class="col-md-4 control-label">Join Time</label>
                    <div class="col-md-6">
                        <input type="text" name="" value="{{$user->created_at->format('h:i:s d/m/Y')}}"
                               class="form-control" readonly>
                        <br>
                        <input type="submit" class="form-control btn btn-primary" value="Update">
                    </div>
                </div>
            </form>
            <hr>
            <div class="col-lg-12">
                <h3>Bill History</h3>
                @php $bills = $user->Bills @endphp
                <table class="table">
                    <thead>
                    <tr>
                        <th>ID</th>
                        <th>User</th>
                        <th>Games</th>
                        <th>Money</th>
                        <th>Buy Time</th>

                    </tr>
                    </thead>
                    <tbody>
                    @if(isset($bills))
                        @php $total=0; @endphp
                        @foreach($bills as $bill)
                            <tr>
                                <td>{{$bill->id}}</td>
                                <td>{{  $bill->User()->withTrashed()->first()->email }}</td>
                                @php

                                        @endphp
                                <td>
                                    @foreach($bill->Keys()->withTrashed()->get() as $key)
                                        {{$key->Game->name.": ".$key->key}} <br/>
                                    @endforeach
                                </td>
                                @php $total+=$bill->money; @endphp
                                <td>{{number_format($bill->money)}}</td>

                                <td>{{ $bill->created_at->format('h:i:s d/m/Y')}}</td>
                            </tr>
                        @endforeach
                        <tr>
                            <td></td>
                            <td></td>
                            <td style="font-size: 22px; font-weight: bold;">Total</td>
                            <td style="font-size: 22px; font-weight: bold;">{{number_format($total)}} VNĐ</td>
                            <td></td>
                        </tr>
                    @endif

                    </tbody>
                </table>
            </div>

        @endif
    </div>
@endsection