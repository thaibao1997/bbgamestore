@extends('app.AppLayout')

@section('current_section')
    @if(isset($game))
        {{$game->name}}
    @endif
@endsection

@section('content')
    <h4 class="alert-success">{{ session('success')  }}</h4>
    <h4 class="alert-danger">{{ session('error')  }}</h4>
    @if(isset($game))
        <div class="col-md-4 agileinfo_single_left">
            <img id="example" src="{{ url($game->Images->first()->path) }}" alt=" " class="img-responsive"/>
        </div>
        <div class="col-md-8 agileinfo_single_right">
            <div class="rating1">
						<span class="starRating">
							<input id="rating5" type="radio" name="rating" value="5" checked>
							<label for="rating5">5</label>
							<input id="rating4" type="radio" name="rating" value="4">
							<label for="rating4">4</label>
							<input id="rating3" type="radio" name="rating" value="3">
							<label for="rating3">3</label>
							<input id="rating2" type="radio" name="rating" value="2">
							<label for="rating2">2</label>
							<input id="rating1" type="radio" name="rating" value="1">
							<label for="rating1">1</label>
						</span>
            </div>
            <div class="w3agile_description">
                <h4>Public Year: <span style="font-weight: normal">{{$game->public_year}}</span></h4>
            </div>
            <div class="w3agile_description">
                <h4>Company: <span style="font-weight: normal; text-transform: capitalize">{{$game->company}}</span>
                </h4>
            </div>

            <div class="w3agile_description">
                <h4>Categories:
                    <span style="font-weight: normal; text-transform: capitalize; ">
                        |
                        @foreach($game->categories as $categoryG)
                            <a href="{{route('app.home.category',$categoryG->id)}}">{{$categoryG->name}}</a> |
                        @endforeach
                    </span>
                </h4>
            </div>


            <div class="w3agile_description">
                <h4>Description :</h4>
                <p>{{$game->description}}</p>
            </div>
            <div class="snipcart-item block">
                <div class="snipcart-thumb agileinfo_single_right_snipcart">

                    <h4 style="font-size: 30px"> {{number_format($game->price)}} VNĐ</h4>
                </div>
                <div class="snipcart-details agileinfo_single_right_details">
                    @if($game->Keys->count() > 0)
                        <form action="{{route('app.cart.add')}}" method="post">
                            <fieldset>
                                {{csrf_field()}}
                                <input type="hidden" name="game" value="{{$game->id}}"/>
                                <input type="submit" name="submit" value="Add to cart"
                                       class="button"/>
                            </fieldset>
                        </form>
                    @else
                        <input type="button" name="submit" value="Not Avaible"
                               class="button" style="background-color: #9B9797"/>
                    @endif
                </div>
            </div>
        </div>
    @endif
    <div class="clearfix"></div>
    <hr>
    <div class="well">
        <h4>Leave a Comment:</h4>
        <form method="post" action="{{route('app.game.comment',$game->id)}}">
            {{csrf_field()}}
            <div class="form-group">
                <textarea name="content" class="form-control" rows="3"></textarea>
            </div>
            <button type="submit" class="btn btn-primary">Submit</button>
        </form>
    </div>

    <hr>

    <!-- Posted Comments -->

    <!-- Comment -->
    @if(isset($comments))
        @foreach($comments as $comment)
            <div class="media">
                @php $commentUser = $comment->User  @endphp
                <div class="media-body">
                    <h4 class="media-heading">{{$commentUser->email}}
                        <small>{{$comment->created_at}}
                            @if(Auth::check() && Auth::user()->role->name==="admin")
                            <form method="POST" action="{{route('admin.comment.delete',$comment->id)}}" accept-charset="UTF-8" onsubmit="return confirm('are you sure?')">
                                <input name="_method" type="hidden" value="DELETE">
                                {{csrf_field()}}
                                <input class="btn btn-danger btn-xs arrow pull-right" type="submit" value="Delete">
                            </form>
                            @endif
                        </small>

                    </h4>
                    {{$comment->content}}
                </div>
            </div>
        @endforeach
        {{$comments->links()}}
    @endif



@endsection