@extends('app.AppLayout')

@section('current_section')
    Thank You
@endsection


@section('content')
   <div>
       <p style="text-align: center; font-family: 'Arial'; font-size: 40px">Cảm ơn bạn đã mua game tại BigBang GameStore. Key Game của bạn đã được gửi vào mail hoặc
       bạn có thể xem ngay tại <a href="{{route('app.user')}}">profile</a> của bạn</p>
   </div>
@endsection