@extends('app.AppLayout')

@section('current_section')
    Cart
@endsection

@section('content')
    <h4 class="alert-danger">{{ session('error')  }}</h4>
    <div class="checkout-right">
        @if(isset($items))
            <h4>Your shopping cart contains: <span>{{$items->count()}}</span></h4>
        @endif
        <table class="timetable_sub">
            <thead>
            <tr>
                <th>SL No.</th>
                <th>Product</th>
                <th>Quality</th>
                <th>Product Name</th>

                <th>Price</th>
                <th>Remove</th>
            </tr>
            </thead>
            <tbody>
            @if(isset($items))
                @php $total =0 ; $count=0; @endphp
                @foreach($items as $item)
                    <tr class="rem1">
                        @php  $game=App\Game::findOrFail($item->id); $count++;  $total += $item->price*$item->qty;    @endphp
                        <td class="invert">{{$count}}</td>

                        <td class="invert-image" width="250"><a href="{{route('app.game',$game->id)}}"><img
                                        src="{{ url($game->Images->first()->path) }}" alt=" "></a></td>
                        <td class="invert">
                            <div class="quantity">
                                <div class="quantity-select">
                                    <a href="{{route('app.cart.decrease',$item->rowId)}}"><button class="entry value-minus active"></button></a>
                                    <div class="entry  value"><span>{{$item->qty}}</span></div>
                                    <a href="{{route('app.cart.increase',$item->rowId)}}"><button class="entry value-plus active"></button></a>
                                </div>
                            </div>
                        </td>
                        <td class="invert">{{$item->name}}</td>

                        <td class="invert">{{number_format($item->price)}}</td>
                        <td class="invert" width="30">
                            <div class="rem">
                                <a href="{{route('app.cart.remove',$item->rowId)}}"><div class="close1"></div></a>
                            </div>

                        </td>
                    </tr>

                @endforeach
            @endif


            </tbody>
        </table>
    </div>
    @if(isset($items))
        <div class="checkout-left">
            <div class="col-md-4 checkout-left-basket">

                <h3>Total <i>:</i> <span>{{number_format($total)}} VNĐ</span></h3>

            </div>
            @if($total > 0)
            <div class="col-md-8 address_form_agile">
                <h4><a href="{{route('app.checkout',$game->id)}}">Make a Payment <span class="glyphicon glyphicon-chevron-right"
                                                                aria-hidden="true"></span></a></h4>
            </div>
            @endif
        </div>
    @endif

    <div class="clearfix"></div>

@endsection