
@if(isset($games))
    @php
        $sort=isset($_GET['sort']) ? $_GET['sort'] : "" ;
    @endphp
    <form action="#" method="GET" class="form-inline">
        <select name="sort" class="form-control">
            <option value="AZ" @php if($sort=="AZ") echo "Selected" @endphp >Tên A -> Z</option>
            <option value="ZA" @php if($sort=="ZA") echo "Selected" @endphp >Tên Z -> A</option>
            <option value="ASC" @php if($sort=="ASC") echo "Selected" @endphp >Giá Tăng Dần</option>
            <option value="DESC" @php if($sort=="DESC") echo "Selected" @endphp>Giá Giảm Dần</option>
        </select>
        <input type="submit" value="Sắp Xếp" class="btn btn-primary form-control">
    </form>
    <br>
    <h4 class="alert-success">{{ session('success')  }}</h4>
    <h4 class="alert-danger">{{ session('error')  }}</h4>
    <hr>
    @foreach($games as $game)
        <div class="col-md-3 w3ls_w3l_banner_left">
            <div class="hover14 column">
                <div class="agile_top_brand_left_grid w3l_agile_top_brand_left_grid">
                    <div class="agile_top_brand_left_grid1">
                        <figure>
                            <div class="snipcart-item block" style="min-height: 255px">
                                <div class="snipcart-thumb">
                                    <a href="{{route('app.game',$game->id)}}">
                                        <img src="{{ url($game->Images->first()->path) }}" alt=""
                                             height="120px" />
                                    </a>
                                    <br>
                                    <h4 style="text-align: center; overflow: hidden; text-overflow: ellipsis;">{{$game->name}}</h4>
                                    <h4  style="text-align: center">{{number_format($game->price)." VNĐ"}} </h4>
                                </div>
                                <div class="snipcart-details">
                                    @if($game->Keys->count() > 0)
                                        <form action="{{route('app.cart.add')}}" method="post">
                                            <fieldset>
                                                {{csrf_field()}}
                                                <input type="hidden" name="game" value="{{$game->id}}"/>
                                                <input type="submit" name="submit" value="Add to cart"
                                                       class="button"/>
                                            </fieldset>
                                        </form>
                                    @else
                                        <input type="button" name="submit" value="Not Avaible"
                                               class="button" style="background-color: #9B9797"/>
                                    @endif
                                </div>
                            </div>
                        </figure>
                    </div>
                </div>
            </div>
        </div>
    @endforeach
    <div class="col-xs-12 center-block">
        @if(isset($_GET['sort']))
            {{ $games->appends(['sort' => $_GET['sort']])->links() }}
        @else
            {{ $games->links() }}
        @endif
    </div>
@endif