@extends("app.AppLayout")

@section('current_section')
    Checkout
@endsection

@section('content')
    <h4 class="alert-danger">{{ session('error')  }}</h4>
    <div class="checkout-right">
        <div class="col-md-12">
        @include('errors.form_validate')
        </div>
        <!--Horizontal Tab-->
        <div id="parentHorizontalTab">
            <ul class="resp-tabs-list hor_1">
                <li>Thẻ Cào</li>
                <li>Netbanking</li>
                <li>Paypal Account</li>
            </ul>
            <div class="resp-tabs-container hor_1">

                <div>
                    <form action="{{route('app.pay.card')}}" method="post" class="creditly-card-form agileinfo_form">
                        {{csrf_field()}}
                        <section class="creditly-wrapper wthree, w3_agileits_wrapper">
                            <div class="credit-card-wrapper">
                                <div class=" form-group">
                                    <div class="controls">
                                        <label class="control-label">Loại Thẻ</label><br>
                                        <input type="radio" name="type" value="Vinaphone"> <label>Vinaphone</label><br>
                                        <input type="radio" name="type" value="Mobifone"> <label>Mobifone</label><br>
                                        <input type="radio" name="type" value="Vietel"> <label>Vietel</label><br>
                                        <input type="radio" name="type" value="Vietnammobile"> <label>Vietnammobile</label><br>
                                    </div>

                                    <div class="controls">
                                        <label class="control-label">Mã Thẻ</label>
                                        <input class="billing-address-name form-control" type="number" name="code" required="">
                                    </div>
                                    <div class="controls">
                                        <label class="control-label">Serial</label>
                                        <input class="billing-address-name form-control" type="number" name="serial" required="">
                                    </div>
                                </div>
                            </div>
                        </section>
                        <input class="btn btn-primary" type="submit" value="PAY NOW">

                    </form>

                </div>
                <div>
                    <div class="vertical_post">
                        <form action="{{route('app.pay.bank')}}" method="post">
                            {{csrf_field()}}
                            <h5>Select From Popular Banks</h5>
                            <div class="swit-radio">
                                <div class="check_box_one">
                                    <div class="radio_one"><label><input type="radio" name="bank" checked=""><i></i>Agri
                                            Bank</label></div>
                                </div>
                                <div class="check_box_one">
                                    <div class="radio_one"><label><input type="radio" name="bank"><i></i>ACB</label>
                                    </div>
                                </div>
                                <div class="check_box_one">
                                    <div class="radio_one"><label><input type="radio" name="bank"><i></i>VP Bank</label></div>
                                </div>
                                <div class="check_box_one">
                                    <div class="radio_one"><label><input type="radio" name="bank"><i></i>DongA Bank</label></div>
                                </div>
                                <div class="check_box_one">
                                    <div class="radio_one"><label><input type="radio" name="bank"><i></i>HSBC</label></div>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <h5>Or SELECT OTHER BANK</h5>
                            <div class="section_room_pay">
                                <select class="year" name="ortherBank[]">
                                    <option value="">=== Other Banks ===</option>
                                    <option value="ALB-NA">Citibank</option>
                                    <option value="ADB-NA">Nam A Bank</option>
                                    <option value="BBK-NA">Maritime Bank</option>
                                </select>
                            </div>
                            <input type="submit" value="PAY NOW">
                        </form>
                    </div>
                </div>
                <div>
                    <div id="tab4" class="tab-grid" style="display: block;">
                        <div class="row">
                            <div class="col-md-6">
                                <img class="pp-img" src="{{asset('app/images/paypal.png')}} " alt="Image Alternative text"
                                     title="Image Title">
                                <p>Important: You will be redirected to PayPal's website to securely complete your
                                    payment.</p><a class="btn btn-primary">Checkout via Paypal</a>
                            </div>
                            <div class="col-md-6">
                                <form class="cc-form" action="{{route('app.pay.paypal')}}" method="post">
                                    {{csrf_field()}}
                                    <div class="clearfix">
                                        <div class="form-group form-group-cc-number">
                                            <label>Card Number</label>
                                            <input name="cardnumber" class="form-control" placeholder="xxxx xxxx xxxx xxxx"
                                                   type="text" required=""><span class="cc-card-icon"></span>
                                        </div>
                                        <div class="form-group form-group-cc-cvc">
                                            <label>CVV</label>
                                            <input name="cvv" class="form-control" placeholder="xxxx" type="text" required="">
                                        </div>
                                    </div>
                                    <div class="clearfix">
                                        <div class="form-group form-group-cc-name">
                                            <label>Card Holder Name</label>
                                            <input name="holdername" class="form-control" type="text" required="">
                                        </div>
                                        <div class="form-group form-group-cc-date">
                                            <label>Valid Thru</label>
                                            <input name="validthru" class="form-control" placeholder="mm/yy" type="text" required="">
                                        </div>
                                    </div>
                                    <input class="btn btn-primary submit" type="submit" class="submit"
                                           value="Proceed Payment">
                                </form>
                            </div>
                        </div>

                    </div>
                </div>

            </div>
        </div>

        <!--Plug-in Initialisation-->

        <!-- // Pay -->

    </div>
@endsection

@section('footer')
    <!-- js -->
    <script src="{{asset('app/js/jquery-1.11.1.min.js')}}"></script>
    <!-- easy-responsive-tabs -->
    <link rel="stylesheet" type="text/css" href="{{asset('app/css/easy-responsive-tabs.css')}}"/>
    <script src="{{asset('app/js/easyResponsiveTabs.js')}}"></script>
    <!-- //easy-responsive-tabs -->
    <script type="text/javascript">
        $(document).ready(function () {
            //Horizontal Tab
            $('#parentHorizontalTab').easyResponsiveTabs({
                type: 'default', //Types: default, vertical, accordion
                width: 'auto', //auto or any width like 600px
                fit: true, // 100% fit in a container
                tabidentify: 'hor_1', // The tab groups identifier
                activate: function (event) { // Callback function if tab is switched
                    var $tab = $(this);
                    var $info = $('#nested-tabInfo');
                    var $name = $('span', $info);
                    $name.text($tab.text());
                    $info.show();
                }
            });
        });
    </script>
    <!-- credit-card -->
    <script type="text/javascript" src="{{asset('app/js/creditly.js')}}"></script>
    <link rel="stylesheet" href="{{asset('app/css/creditly.css')}}" type="text/css" media="all"/>

    <script type="text/javascript">
        $(function () {
            var creditly = Creditly.initialize(
                '.creditly-wrapper .expiration-month-and-year',
                '.creditly-wrapper .credit-card-number',
                '.creditly-wrapper .security-code',
                '.creditly-wrapper .card-type');

            $(".creditly-card-form .submit").click(function (e) {
                e.preventDefault();
                var output = creditly.validate();
                if (output) {
                    // Your validated credit card output
                    console.log(output);
                }
            });
        });
    </script>
    <!-- //credit-card -->

    <!-- //js -->
    <!-- script-for sticky-nav -->
    <script>
        $(document).ready(function () {
            var navoffeset = $(".agileits_header").offset().top;
            $(window).scroll(function () {
                var scrollpos = $(window).scrollTop();
                if (scrollpos >= navoffeset) {
                    $(".agileits_header").addClass("fixed");
                } else {
                    $(".agileits_header").removeClass("fixed");
                }
            });

        });
    </script>
    <!-- //script-for sticky-nav -->
    <!-- start-smoth-scrolling -->
    <script type="text/javascript" src="{{asset('app/js/move-top.js') }}"></script>
    <script type="text/javascript" src="{{asset('app/js/easing.js')}}"></script>
    <script type="text/javascript">
        jQuery(document).ready(function ($) {
            $(".scroll").click(function (event) {
                event.preventDefault();
                $('html,body').animate({scrollTop: $(this.hash).offset().top}, 1000);
            });
        });
    </script>

@endsection