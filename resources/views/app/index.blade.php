@extends('app.AppLayout')

@section('current_section')
    All Games
@endsection

@section('banner1')
    <div class="flexslider">
        <ul class="slides">
            <li>
                <div class="w3l_banner_nav_right_banner">
                    <h3>Make your <span>food</span> with Spicy.</h3>
                    <div class="more">
                        <a href="products.html" class="button--saqui button--round-l button--text-thick" data-text="Shop now">Shop now</a>
                    </div>
                </div>
            </li>
            <li>
                <div class="w3l_banner_nav_right_banner1">
                    <h3>Make your <span>food</span> with Spicy.</h3>
                    <div class="more">
                        <a href="products.html" class="button--saqui button--round-l button--text-thick" data-text="Shop now">Shop now</a>
                    </div>
                </div>
            </li>
        </ul>
        <link rel="stylesheet" href="{{asset('app/css/flexslider.css')}} " type="text/css" media="screen" property="" />
        <script defer src="{{asset('app/js/jquery.flexslider.js')}}"></script>
        <script type="text/javascript">
            $(window).load(function(){
                $('.flexslider').flexslider({
                    animation: "slide",
                    start: function(slider){
                        $('body').removeClass('loading');
                    }
                });
            });
        </script>
    </div>
@endsection

@section('content')
        @include('app.showGames')
@endsection