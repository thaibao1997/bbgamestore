<div class="collapse navbar-collapse" id="bs-megadropdown-tabs">
    <ul class="nav navbar-nav nav_1">

        @if(isset($categories))
            @foreach($categories as $category_)
                @if(isset($category) && $category->id == $category_->id)
                    <li style="background-color: #5cb85c"><a href="{{route('app.home.category',$category_->id)}}"><h3>{{$category_->name}}</h3></a></li>
                @else
                    <li ><a href="{{route('app.home.category',$category_->id)}}"><h4>{{$category_->name}}</h4></a></li>
                @endif
            @endforeach
        @endif

    </ul>
</div><!-- /.navbar-collapse -->
