@extends('app.AppLayout')

@section('current_section')
    @if(isset($category))
        {{$category->name}}
    @endif
@endsection


@section('content')
    @include('app.showGames')
@endsection