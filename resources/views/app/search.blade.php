@extends('app.AppLayout')

@section('current_section')
    @if(isset($games))
        {{$games->count()}} Results for "{{ $_GET['name'] }}"
    @endif
@endsection



@section('content')
    @include('app.showGames')
@endsection