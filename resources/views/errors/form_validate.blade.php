@if(count($errors) > 0)
    <div class="alert alert-danger">
        <ol>
        @foreach($errors->all() as $err)
            <li>{{$err}}</li>
        @endforeach
        </ol>
    </div>
@endif