<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Image extends Model
{
    //
    protected $fillable=['path'];
    public $timestamps=false;

    public function Game(){
        return $this->belongsTo('App\Game');
    }
}
