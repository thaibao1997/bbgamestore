<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    //
    protected $fillable=['content'];
    public function Game(){
        return $this->belongsTo('App\Game');
    }

    public function User(){
        return $this->belongsTo('App\User');
    }
}
