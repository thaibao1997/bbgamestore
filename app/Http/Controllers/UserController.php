<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Category;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    //
    public function Show(){
        if(!Auth::check()){
            return redirect()->route('app.home');
        }
        $categories=Category::all();
        $user=Auth::user();
        return view('app.userprofile',compact(['user','categories']));
    }
    public function Update(Request $request){
        $request->validate([
            'name' => 'required',
        ]);
        $user=Auth::user();
        $user->update($request->all());
        $user->save();
        return redirect()->back()->with('status','User\'s Profile Has Been Update');
    }
}
