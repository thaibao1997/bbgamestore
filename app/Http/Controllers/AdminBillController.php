<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Bill;
use Illuminate\Support\Facades\DB;

class AdminBillController extends Controller
{
    //
    public function Show(){
        $bills=Bill::all();
        return view('admin.bill.index',compact('bills'));
    }

    public function Statistic(){
        $bills=Bill::all();
        $bills=DB::table('bills')->select(DB::raw(' CONCAT(Month(created_at),"-",Year(created_at))  as date'),DB::raw('count(*) as bill_count'),DB::raw('sum(money) as total'))
                ->orderBy('created_at')->groupBy('date')
                ->get();
        $games=DB::select(DB::raw('
          SELECT G.id,G.name, COUNT(*) AS SL
          FROM (`games` AS G JOIN `keys` AS K on G.id=K.game_id) JOIN `bill_key` AS B ON B.key_id=K.id
          GROUP BY G.id,G.name
        '));
        $games = json_decode(json_encode($games), true);
        usort($games,create_function('$a,$b','
                return $b["SL"] - $a["SL"];
        '));
        return view('admin.statistic',compact(['bills','games']));
    }

    public function StatisticDate(Request $request){

    }
}
