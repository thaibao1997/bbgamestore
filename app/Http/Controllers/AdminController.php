<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AdminController extends Controller
{
    //
    public function Show(){
        $games=\App\Game::all()->count();
        $comments=\App\Comment::all()->count();
        $users=\App\User::all()->count();
        $bills = \App\Bill::all()->count();
        return view('admin.index',compact(['games','comments','users','bills']));
    }
}
