<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Game;
use  App\Category;
use App\Comment;
use Illuminate\Support\Facades\Auth;


class GameController extends Controller
{
    //
    public function Show($id){
        $game = Game::findOrFail($id);
        $categories=Category::all();
        $comments=$game->Comments()->paginate(10);
        return view('app/game',compact(['categories','game','comments']));
    }

    public function Comment(Request $request,$id){
        $comment = new Comment($request->all());
        $comment->Game()->associate($id);
        $comment->User()->associate(Auth::user()->id);
        $comment->save();
        return redirect()->back();
    }
}
