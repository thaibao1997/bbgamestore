<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use \App\Game;
use \App\Category;
use \App\Image;
use \App\Key;

class AdminGameController extends Controller
{
    //
    public function Show(){
        $games=Game::all();
        return view('admin.game.index',['games'=>$games]);
    }

    public function Destroy($id){
        $game=Game::findOrFail($id);
        //$this->authorize('destroy',$user);
        $game->delete();
        session()->flash('status','Games has been deleted');
        return redirect()->back();
    }

    public function Create(){
        $categories = Category::all();
        return view('admin.game.create',compact('categories'));
    }
    public function Store(Request $request){
        $request->validate([
            'name' => 'required|unique:categories|max:191',
            'price' =>'required|numeric',
            'description'=>'required',
            'public_year'=>'nullable|numeric',
            'image'=>'required',
            'categoriesID' => 'required',
        ]);

        $game= new Game($request->all());
        $game->save();

        $ImagePath=public_path().'\\Images\\';

        if($request->hasFile('image')){
            $file = $request->file('image');
            $ImageName= $game->id.'_'.time().'.'.$file->getClientOriginalExtension();
            $file->move($ImagePath, $ImageName);
            $game->Images()->create(['path'=>'\\Images\\'.$ImageName]);
        }

        $categoriesID = $request->input('categoriesID');
        foreach ($categoriesID as $id){
            $game->Categories()->attach($id);
        }

        $keys=$request->input('keys');
        if(!empty($keys)){
            $keyArray=explode("\r\n",$keys);
            foreach ($keyArray as $key){
                if($game->Keys()->where('key',$key)->get()->isEmpty()) {
                    $game->Keys()->create(['key' => $key]);
                }
            }
        }

        return redirect()->back()->with('status','Game Has Been Added');

    }

    public function Edit($id){
        $game=Game::findOrFail($id);
        $categories = Category::all();
        return view('admin.game.edit',compact(['game','categories']));
    }

    public function Update(Request $request,$id){
        $request->validate([
            'name' => 'required|unique:categories|max:191',
            'price' =>'required|numeric',
            'public_year'=>'nullable|numeric',
            'categoriesID' => 'required',
            'description'=>'required',
        ]);
        $game=Game::findOrFail($id);
        $game->update($request->all());
        $game->save();

        if($request->hasFile('image')){
            $game->Images()->delete();
            $ImagePath=public_path().'\\Images\\';
            $file = $request->file('image');
            $ImageName= $game->id.'_'.time().'.'.$file->getClientOriginalExtension();
            $file->move($ImagePath, $ImageName);
            $game->Images()->create(['path'=>'\\Images\\'.$ImageName]);
        }

        $game->Categories()->detach();

        $categoriesID = $request->input('categoriesID');
        foreach ($categoriesID as $id){
           // if(!$game->Categories()->find($id))
                $game->Categories()->attach($id);
        }

        $keys=$request->input('keys_');
        if(!empty($keys)){
            $keyArray=explode("\r\n",$keys);
            foreach ($keyArray as $key){
                if($game->Keys()->where('key',$key)->get()->isEmpty()) {
                    $game->Keys()->create(['key' => $key]);
                }
            }
        }
        return redirect()->back()->with('status','Game Has Been Updated');

    }

    public function KeyShow(){
        $keys= Key::All();
        return view('admin.game.key',compact('keys'));
    }

    public function KeyDelete($id){
        $key=Key::findOrFail($id);
        $key->delete();
        return redirect()->back()->with('status','Key Has Been Delete');
    }
}
