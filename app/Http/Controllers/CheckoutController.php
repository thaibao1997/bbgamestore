<?php

namespace App\Http\Controllers;

use Illuminate\Mail\Mailer;
use Gloudemans\Shoppingcart\Facades\Cart;
use Illuminate\Http\Request;
use App\Category;
use App\Bill;
use App\Game;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use App\Mail\UserEmail;


class CheckoutController extends Controller
{
    //
    public function Show(){
        $categories = Category::all();

        return view('app/checkout',compact('categories'));
    }

    public function Thankyou(){
        $categories = Category::all();

        return view('app/thankyou',compact('categories'));
    }

    public function PayByCard(Request $request){
        $request->validate([
            'type' => 'required',
            'code' => 'required | numeric',
            'serial' => 'required | numeric'
        ]);

        $bill=new Bill();
        $bill->User()->associate(Auth::User()->id);
        $bill->money=     intval(str_replace(',', '',Cart::subtotal()));//Cart::subtotal()*1;
        $bill->save();
        $items=Cart::content();
        foreach ($items as $item){
            $game=Game::findOrFail($item->id);
            for($i=0;$i<$item->qty;$i++) {
                $keys = $game->Keys()->first();
                $bill->Keys()->attach($keys->id);
                $keys->delete();
            };
        }
        Cart::destroy();
        return redirect()->Route('app.thankyou');
    }

    public function PayByBank(Request $request){
        $request->validate([
            'ortherBank' => 'required_without:bank',
        ]);

        $bill=new Bill();
        $bill->User()->associate(Auth::User()->id);
        $bill->money=     intval(str_replace(',', '',Cart::subtotal()));//Cart::subtotal()*1;
        $bill->save();
        $items=Cart::content();
        foreach ($items as $item){
            $game=Game::findOrFail($item->id);
            for($i=0;$i<$item->qty;$i++) {
                $keys = $game->Keys()->first();
                $bill->Keys()->attach($keys->id);
                $keys->delete();
            }
        }
        Cart::destroy();
        return redirect()->Route('app.thankyou');
    }

    public function PayByPaypal(Request $request){
        $request->validate([
            'cardnumber' => 'required',
            'cvv' => 'required',
            'holdername' => 'required',
            'validthru' => 'required',
        ]);

        $bill=new Bill();
        $bill->User()->associate(Auth::User()->id);
        $bill->money=     intval(str_replace(',', '',Cart::subtotal()));//Cart::subtotal()*1;
        $bill->save();
        $items=Cart::content();
        foreach ($items as $item){
            $game=Game::findOrFail($item->id);
            for($i=0;$i<$item->qty;$i++) {
                $keys = $game->Keys()->first();
                $bill->Keys()->attach($keys->id);
                $keys->delete();
            }
        }
        Cart::destroy();
        Mail::to(Auth::user()->email)->send(new UserEmail());
        return redirect()->Route('app.thankyou');
    }
}
