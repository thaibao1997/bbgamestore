<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Game;
use  App\Category;
use Gloudemans\Shoppingcart\Facades\Cart;


class CartController extends Controller
{
    //

    public function Show(){
        $categories=Category::all();
        $items=Cart::content();
        return view('app/cart',compact(['categories','items']));
    }

    public function AddCart(Request $request){
        $game=Game::findOrFail($request->input('game'));
        $cartItem= Cart::search(
            function ($cartItems,$rowId) use($game) {
                return $cartItems->id == $game->id;
            }
        )->first();
        $cartQty=0;
        if($cartItem!=null)
            $cartQty=$cartItem->qty;
        if($game->Keys()->count() > $cartQty) {
            Cart::add($game->id, $game->name, 1, $game->price);
            return redirect()->back()->with('success','Game added to cart');
        }
        return redirect()->back()->with('error','Can not add any more');
    }

    public function IncreaseQty($row_id){
        $item=Cart::get($row_id);
        $game=Game::findOrFail($item->id);
        if($game->Keys()->count() < $item->qty+1 ) {
            return redirect()->back()->with('error','Can not add any more');
        }
        Cart::update($row_id,$item->qty+1);
        $categories=Category::all();
        $items=Cart::content();
        return redirect()->back();
    }

    public function DecreaseQty($row_id){
        $item=Cart::get($row_id);
        Cart::update($row_id,$item->qty-1);
        $categories=Category::all();
        $items=Cart::content();
        return redirect()->back();
    }

    public function Remove($row_id){
        Cart::remove($row_id);
        return redirect()->back();
    }


}
