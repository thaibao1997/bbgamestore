<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Comment;

class AdminCommentController extends Controller
{
    //
    public function Show(){
        $comments=Comment::all();
        return view('admin.comment.index',compact('comments'));
    }

    public function Destroy($id){
        $comment=Comment::findOrFail($id);
        $comment->delete();
        return redirect()->back()->with('status','Comment has been deleted');
    }
}
