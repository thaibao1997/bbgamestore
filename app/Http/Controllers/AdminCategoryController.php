<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\Category;

class AdminCategoryController extends Controller
{
    //
    public function Show(){
        $categories = Category::all();
        return view('admin.category.index',['categories'=>$categories]);
    }

    public function Destroy($id){
        $category=Category::findOrFail($id);

        $category->delete();
        return redirect()->route('admin.category')->with('delete','Category has been deleted');
    }

    public function Store(Request $request){
        $request->validate([
            'name' => 'required|unique:categories|max:191',
        ]);
        if (Category::create($request->all())) {
            return redirect()->back()->with('success', 'Category Has Been Added');
        }

        return redirect()->back()->with('status','An Error Has Happen');
    }

    public function Edit($id){
        $edit_category = Category::findOrFail($id);
        $categories = Category::all();
        return view('admin.category.index', compact(['categories', 'edit_category']));

    }
    public function Update(Request $request,$id){
        $request->validate([
            'name' => 'required|unique:categories|max:191',
        ]);

        $category=Category::findOrFail($id);
        if ($category->update($request->all())) {
            return redirect()->back()->with('success', 'Category Has Been Updated');
        }
        return redirect()->back()->with('unsuccess', 'An Error Has Happened');    }
}
