<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;
use App\Game;
use Illuminate\Support\Facades\Input;

class AppController extends Controller
{
    //


    public function Home(){

        $categories=Category::all();
        if(Input::get('sort'))
            $games= $this::GetSortedGames(Input::get('sort'));
        else
            $games=Game::paginate(20);

        return view('app/index',compact(['games','categories']));
    }

    public function GameByCategory($id){
        $categories=Category::all();
        $category=Category::findOrFail($id);
        if(Input::get('sort'))
            switch (Input::get('sort')) {
                case 'AZ':
                    $games=$category->Games()->orderBy('name')->paginate(20);
                    break;
                case 'ZA':
                    $games=$category->Games()->orderBy('name','desc')->paginate(20);
                    break;
                case 'ASC':
                    $games=$category->Games()->orderBy('price')->paginate(20);
                    break;
                case 'DESC':
                    $games=$category->Games()->orderBy('price','desc')->paginate(20);
                    break;
                default:
                    $games=$category->Games()->paginate(20);
            }
        else
            $games=$category->Games()->paginate(20);

        return view('app/category',compact(['games','categories','category']));
    }

    public function Search(Request $request){
        $name= $request->input('name');
        $categories=Category::all();
        if(Input::get('sort'))
            switch (Input::get('sort')) {
                case 'AZ':
                    $games=Game::search($name)->orderBy('name')->paginate(20);
                    break;
                case 'ZA':
                    $games=Game::search($name)->orderBy('name','desc')->paginate(20);
                    break;
                case 'ASC':
                    $games=Game::search($name)->orderBy('price')->paginate(20);
                    break;
                case 'DESC':
                    $games=Game::search($name)->orderBy('price','desc')->paginate(20);
                    break;
                default:
                    $games=Game::search($name)->paginate(20);
            }
        else
            $games=Game::search($name)->paginate(20);
        return view('app.search',compact(['games','categories']));
    }

    public static function GetSortedGames($type){
        switch ($type) {
            case 'AZ':
                return Game::orderBy('name')->paginate(20);
                break;
            case 'ZA':
                return Game::orderBy('name','desc')->paginate(20);
                break;
            case 'ASC':
                return Game::orderBy('price')->paginate(20);
                break;
            case 'DESC':
                return Game::orderBy('price','desc')->paginate(20);
                break;
            default:
                return Game::paginate(20);
        }
    }

    public function Login(){
        $categories=Category::all();
        return view('app.login',compact(['categories']));

    }

}
