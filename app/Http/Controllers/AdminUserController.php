<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\User;

class AdminUserController extends Controller
{
    //
    //
    public function Show(){
        $users= User::orderBy('role_id')->get();
        return view('admin.user.index',['users'=>$users]);
    }

    public function Destroy($id){
        $user=User::findOrFail($id);
        //$this->authorize('destroy',$user);
        if($user->role->name !== 'admin') {
            $user->delete();
            return redirect()->back()->with('status', 'user has been deleted');
        }
        return redirect()->back()->with('status', 'can not delete admin');
    }
}
