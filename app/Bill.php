<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Bill extends Model
{
    //
    use SoftDeletes;
    protected $fillable=['money'];
    protected $dates = ['deleted_at'];

    public function Games(){
        return $this->hasManyThrough('App\Game','App\Key');
    }

    public function User(){
        return $this->belongsTo('App\User');
    }

    public function Keys(){
        return $this->belongsToMany('App\Key');
    }


}
