<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Key extends Model
{
    //
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $fillable=['key'];
    public $timestamps=false;

    public function Game(){
        return $this->belongsTo('App\Game');
    }


}
