<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Category extends Model
{
    use SoftDeletes;
    //
    protected $fillable=['name'];
    public $timestamps = false;


    protected $dates = ['deleted_at'];

    public function Games(){
        return $this->belongsToMany('App\Game');
    }
}
