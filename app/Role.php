<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    //
    protected $table='Roles';


    public function Users(){
        return $this->hasMany('App\User');
    }
}
