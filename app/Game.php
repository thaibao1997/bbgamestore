<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use Illuminate\Database\Eloquent\SoftDeletes;
use Laravel\Scout\Searchable;

class Game extends Model
{
    //
    use SoftDeletes;
    use Searchable;

    protected $dates = ['deleted_at'];

    protected $fillable=['name','price','public_year','company','description'];


    public function Bills(){
       return $this->hasManyThrough('App\Bill','App\Key');
    }

    public function Categories(){
        return $this->belongsToMany('App\Category');
    }

    public function Comments(){
        return $this->hasMany('App\Comment');
    }
    public function Images(){
        return $this->hasMany('App\Image');
    }

    public function Keys(){
        return $this->hasMany('App\Key');
    }


}
