<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'AppController@Home')->name('app.home');

Auth::routes();

Route::get('/category/{id}', 'AppController@GameByCategory')->name('app.home.category');

Route::get('/search','AppController@Search')->name('app.home.search');

Route::get('/Cart','CartController@Show')->name('app.cart');

Route::post('/Cart/Add','CartController@AddCart')->name('app.cart.add');

Route::get('/Cart/{row_id}/inc','CartController@IncreaseQty')->name('app.cart.increase');

Route::get('/Cart/{row_id}/des','CartController@DecreaseQty')->name('app.cart.decrease');

Route::get('/Cart/{row_id}/remove','CartController@Remove')->name('app.cart.remove');

Route::get('/Game/{id}','GameController@Show')->name('app.game');

Route::post('/Game/{id}/comment','GameController@Comment')->name('app.game.comment');

Route::get('/home', 'HomeController@index')->name('home');


Route::middleware(['auth', 'admin'])->group(function () {

    Route::get('/admin', 'AdminController@Show')->name('admin.index') ;

    Route::get('admin/user','AdminUserController@Show')->name('admin.user');

    Route::delete('admin/user/{id}','AdminUserController@Destroy')->name('admin.user.delete');


    Route::get('admin/game','AdminGameController@Show')->name('admin.game');

    Route::get('admin/game/create','AdminGameController@Create')->name('admin.game.create');

    Route::post('admin/game/create','AdminGameController@Store')->name('admin.game.store');

    Route::delete('admin/game/{id}','AdminGameController@Destroy')->name('admin.game.delete');

    Route::get('admin/game/edit/{id}','AdminGameController@Edit')->name('admin.game.edit');

    Route::put('admin/game/edit/{id}','AdminGameController@Update')->name('admin.game.update');

    Route::get('admin/game/keys','AdminGameController@KeyShow')->name('admin.game.key');

    Route::delete('admin/game/key/{id}','AdminGameController@KeyDelete')->name('admin.game.key.delete');


    Route::get('admin/category','AdminCategoryController@Show')->name('admin.category');

    Route::delete('admin/category/{id}','AdminCategoryController@Destroy')->name('admin.category.delete');

    Route::post('admin/category','AdminCategoryController@Store')->name('admin.category.store');

    Route::get('admin/category/{id}','AdminCategoryController@Edit')->name('admin.category.edit');

    Route::put('admin/category/{id}','AdminCategoryController@Update')->name('admin.category.update');


    Route::get('admin/comment','AdminCommentController@Show')->name('admin.comment');

    Route::delete('admin/comment/{id}','AdminCommentController@Destroy')->name('admin.comment.delete');

    Route::get('admin/bill','AdminBillController@Show')->name('admin.bill');

    Route::get('admin/statistic','AdminBillController@Statistic')->name('admin.statistic');

    Route::post('admin/statistic','AdminBillController@StatisticDate')->name('admin.statistic.date');

});

Route::middleware(['auth'])->group(function () {

    Route::get('/user','UserController@Show')->name('app.user');

    Route::put('user/update','UserController@Update')->name('app.user.update');

    Route::get('/checkout','CheckoutController@Show')->name('app.checkout');

    Route::post('/Checkout/Cart','CheckoutController@PayByCard')->name('app.pay.card');

    Route::post('/Checkout/bank','CheckoutController@PayByBank')->name('app.pay.bank');

    Route::post('/Checkout/paypal','CheckoutController@PayByPaypal')->name('app.pay.paypal');

    Route::get('/thankyou','CheckoutController@Thankyou')->name('app.thankyou');

    Route::get('/logout','Auth\LoginController@logout')->name('app.logout');

});