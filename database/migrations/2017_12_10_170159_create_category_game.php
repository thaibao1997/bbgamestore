<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCategoryGame extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('category_game', function (Blueprint $table) {
            $table->engine = 'InnoDB';

            //
            $table->increments('id');
            $table->integer('category_id')->unsigned()->notnull();
            $table->integer('game_id')->unsigned()->notnull();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('category_game', function (Blueprint $table) {
            //
        });
    }
}
